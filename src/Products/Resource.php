<?php

namespace ShandiaLamp\HuaWeiCloud\Products;

use ShandiaLamp\HuaWeiCloud\Client;

class Resource
{
    protected $client;
    protected $productPath;

    public function __construct(Client $client, string $productPath)
    {
        $this->client = $client;
        $this->productPath = $productPath;
    }

    protected function api($method, $api, $data = null)
    {
        return $this->client->api($method, $this->productPath.$api, $data);
    }
}
