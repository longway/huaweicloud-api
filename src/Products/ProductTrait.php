<?php

namespace ShandiaLamp\HuaWeiCloud\Products;

use Closure;
use ShandiaLamp\HuaWeiCloud\ClientException;
use ShandiaLamp\HuaWeiCloud\Products\ECS\ECS;

trait ProductTrait
{
    protected $product;
    protected $productInstances = [];

    public function ecs() : ECS
    {
        return $this->getProduct('ecs');
    }

    protected function getProduct($name)
    {
        if (!isset($this->productInstances[$name])) {
           throw new ClientException("未找到[$name]产品"); 
        }
        $this->product = $this->productInstances[$name];
        return $this->product;
    }

    protected function registerProduct($name, Product $product)
    {
        $this->productInstances[$name] = $product;
    }
}
