<?php

namespace ShandiaLamp\HuaWeiCloud\Products\ECS;

use GuzzleHttp\Client;
use ShandiaLamp\HuaWeiCloud\Products\Resource;

class Flavor extends Resource
{
    public function get()
    {
        ///v1/{project_id}/cloudservers/flavors{?availability_zone}
        return $this->api('GET', '/flavors');
    }
}
