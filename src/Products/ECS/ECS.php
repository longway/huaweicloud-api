<?php

namespace ShandiaLamp\HuaWeiCloud\Products\ECS;

use ShandiaLamp\HuaWeiCloud\Products\Product;

class ECS extends Product
{
    protected $name = 'ecs';
    protected $flavor;

    public function prefixPath() : string
    {
        $projectID = $this->client->getProjectID();
        return "/{$projectID}/cloudservers";
    }

    public function flavor() : Flavor
    {
        return $this->makeResource('flavor', function () {
            return new Flavor($this->getClient(), $this->prefixPath());
        });
    }
}
