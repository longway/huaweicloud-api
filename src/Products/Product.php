<?php

namespace ShandiaLamp\HuaWeiCloud\Products;

use Closure;
use ShandiaLamp\HuaWeiCloud\Client;

abstract class Product
{
    protected $name;
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    abstract protected function prefixPath() : string;

    /**
     * 获取产品名称
     * @return string
     */
    public function name()
    {
        return $this->name;
    }

    protected function makeResource($resource, Closure $closure)
    {
        if (is_null($this->$resource)) {
            $this->$resource = call_user_func($closure);
        }
        return $this->$resource;
    }

    protected function getClient()
    {
        return $this->client;
    }
}
