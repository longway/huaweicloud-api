<?php

namespace ShandiaLamp\HuaWeiCloud;

use GuzzleHttp\Client as HTTPClient;
use GuzzleHttp\Exception\RequestException;
use ShandiaLamp\HuaWeiCloud\Products\ECS\ECS;
use ShandiaLamp\HuaWeiCloud\Products\ProductTrait;

class Client
{
    use ProductTrait;

    const HOST = 'myhuaweicloud.com';

    protected $scheme = 'https';
    protected $endpoint;
    protected $version = 'v1';
    protected $projectID;
    protected $username;
    protected $password;
    protected $domain;

    protected $token;
    protected $tokenExpiresAt;

    protected $httpClient;

    public function __construct()
    {
        $this->httpClient = new HTTPClient();

        $this->registerProduct('ecs', new ECS($this));
    }

    /**
     * 设置终端节点
     * https://developer.huaweicloud.com/endpoint
     * @param string $endpoint
     * @return Client
     */
    public function setEndpoint(string $endpoint)
    {
        $this->endpoint = $endpoint;
        return $this;
    }

    public function getEndpoint(string $endpoint)
    {
        return $this->endpoint;
    }

    public function setProjectID($projectID)
    {
        $this->projectID = $projectID;
        return $this;
    }

    public function getProjectID()
    {
        return $this->projectID;
    }

    public function setVersion(string $version = 'v1')
    {
        $this->version = $version;
        return $this;
    }

    public function setUser(string $username, string $password, $domain)
    {
        $this->username = $username;
        $this->password = $password;
        $this->domain   = $domain;
        return $this;
    }

    public function api(string $method, string $resourcePath, $data)
    {
        if ( !$this->token || time() > $this->tokenExpiresAt ) $this->token();
        $uri = 'https://'.$this->product->name().'.'.$this->endpoint.'.'.self::HOST.$resourcePath;
        $res = $this->httpClient->request($method, $this->parseURI($resourcePath), [
            'headers' => [
                'Content-Type'  => 'application/json',
                'X-Project-Id'  => $this->projectID,
                'X-Auth-Token'  => $this->token,
            ]
        ]);
        return $res->getBody()->getContents();
    }

    public function token()
    {
        $host = self::HOST;
        $uri = "{$this->scheme}://iam.{$this->endpoint}.{$host}/v3/auth/tokens";

        try {
            $res = $this->httpClient->request('POST', $uri, [
                'headers' => [
                    'Content-Type'  => 'application/json',
                ],
                'json' => [
                    'auth' => [
                        'identity' => [
                            'methods' => ['password'],
                            'password'  => [
                                'user'    => [
                                    'name'      => $this->username,
                                    'password'  => $this->password,
                                    'domain'    => [
                                        'name' => $this->domain,
                                    ],
                                ],
                            ],
                        ],
                        'scope'   => [
                            'domain'  => [
                                'name' => $this->domain,
                            ],
                            'project' => [
                                'name' => $this->endpoint,
                            ],
                        ],
                    ],
                ]
            ]);
            $this->token = $res->getHeader('X-Subject-Token')[0];
            $result = $res->getBody()->getContents();
            $result = json_decode($result);
            $this->tokenExpiresAt = time($result->token->expires_at);
        } catch (RequestException $e) {
            throw new ClientException($e->getMessage());
        }
    }

    private function parseURI(string $resourcePath)
    {
        if (!$this->product) throw new ClientException('尚未选择产品！');
        $scheme = $this->scheme . ':';
        $host = '//' . join('.', [$this->product->name(), $this->endpoint, self::HOST]);
        $path = join('/', ['', $this->version, trim($resourcePath, '/')]);

        return $scheme . $host . $path;
    }
}
