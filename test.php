<?php

use ShandiaLamp\HuaWeiCloud\Client;
use ShandiaLamp\HuaWeiCloud\Components\ECS\Flavor;

require 'vendor/autoload.php';

$client = new Client();
//ecs.cn-north-1.myhuaweicloud.com
$client->setEndpoint('cn-north-1')
       ->setVersion('v1');

$res = $client->ecs()->flavor()->get();

var_dump($res);